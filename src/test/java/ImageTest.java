

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageTest {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        BufferedImage jpg = ImageIO.read(new FileInputStream(args[0]));
        ImageIO.write(jpg, "png", new FileOutputStream(args[1]));
    }

}
