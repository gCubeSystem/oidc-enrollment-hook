package org.gcube.portal.oidc.lr62;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.gcube.oidc.rest.JWTToken;
import org.gcube.oidc.rest.OpenIdConnectConfiguration;
import org.gcube.oidc.rest.OpenIdConnectRESTHelper;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.BaseFilter;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;

public class OpenIdConnectLoginFilter extends BaseFilter {

    protected static final Log log = LogFactoryUtil.getLog(OpenIdConnectLoginFilter.class);

    public static final String REDIRECT_ATTRIBUTE = "redirect-after-login-to";

    public OpenIdConnectLoginFilter() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("Filter created");
        }
    }

    @Override
    protected void processFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws Exception {

        User user;
        try {
            if (log.isDebugEnabled()) {
                log.debug("Getting user via portal utils");
            }
            user = PortalUtil.getUser(request);
        } catch (PortalException | SystemException e) {
            throw new ServletException("Getting user using utils", e);
        }
        HttpSession session = request.getSession(false);
        JWTToken token = null;
        if (user == null) {
            String uri = request.getRequestURI();
            if (log.isDebugEnabled()) {
                log.debug("No user logged in " + uri);
            }
            String sessionId = session.getId();
            OpenIdConnectConfiguration configuration = LiferayOpenIdConnectConfiguration.getConfiguration(request);
            if (request.getParameter("state") != null && request.getParameter("state").equals(sessionId)) {
                try {
                    token = OpenIdConnectRESTHelper.queryToken(request.getServerName(),
                            configuration.getTokenURL(), request.getParameter("code"), configuration.getScope(),
                            request.getRequestURL().toString());
                } catch (Exception e) {
                    throw new ServletException("Querying token from OIDC server", e);
                }
                JWTTokenUtil.putOIDCInRequest(token, request);
                // The auto login class will perform the portal auto login using the token

                // Handling original requested URI
                String redirect = (String) session.getAttribute(REDIRECT_ATTRIBUTE);
                if (redirect != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("Moving existing redirect attribute from session to the request object");
                    }
                    request.setAttribute(REDIRECT_ATTRIBUTE, redirect);
                    session.removeAttribute(OpenIdConnectLoginFilter.REDIRECT_ATTRIBUTE);
                } else if (log.isTraceEnabled()) {
                    log.trace("No redirect attribute is set in session object");
                }
            } else {
                String redirect = getRedirect(request);
                if (redirect != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("Setting orginal requested URI in session: " + redirect);
                    }
                    session.setAttribute(REDIRECT_ATTRIBUTE, redirect);
                }
                String oidcRedirectURL = OpenIdConnectRESTHelper.buildLoginRequestURL(
                        configuration.getAuthorizationURL(), request.getServerName(), sessionId,
                        request.getRequestURL().toString());

                if (log.isDebugEnabled()) {
                    log.debug("Redirecting to OIDC server login URL: " + oidcRedirectURL);
                }
                response.sendRedirect(oidcRedirectURL);
                return;
            }
        }

        processFilter(getClass(), request, response, filterChain);
    }

    private String getRedirect(HttpServletRequest request) {
        if (log.isTraceEnabled()) {
            log.trace("Getting redirect parameter");
        }
        String redirect = request.getParameter("redirect");
        if (redirect == null) {
            if (log.isTraceEnabled()) {
                log.trace("Redirect parameter is null, trying with p_p_id and related redirect parameter");
            }
            String ppId = request.getParameter("p_p_id");
            if (ppId != null) {
                String redirectParamName = "_" + ppId + "_";
                if (log.isTraceEnabled()) {
                    log.trace("Getting numbered redirect parameter: " + redirectParamName);
                }
                redirect = request.getParameter(redirectParamName);
            }
        }
        return redirect;
    }

    @Override
    protected Log getLog() {
        return log;
    }

}
