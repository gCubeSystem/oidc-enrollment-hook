package org.gcube.portal.oidc.lr62;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.gcube.common.portal.PortalContext;
import org.gcube.oidc.rest.JWTToken;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;

public class PostLoginAction extends Action {

    protected static final Log log = LogFactoryUtil.getLog(PostLoginAction.class);

    public static boolean REQUEST_UMA_ALSO_WITH_REDIRECT = true;

    @Override
    public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        if (log.isInfoEnabled()) {
            log.info("PostLoginAction invoked");
        }
        JWTToken token = JWTTokenUtil.getOIDCFromRequest(request);
        HttpSession session = request.getSession(false);
        String redirect = (String) request.getAttribute(OpenIdConnectLoginFilter.REDIRECT_ATTRIBUTE);;
        if (token != null && session != null) {
            User user = (User) session.getAttribute(WebKeys.USER);
            if (user != null) {
                log.info("Setting OIDC token in proxy for user " + user.getScreenName() + " and session "
                        + session.getId());

                JWTCacheProxy.getInstance().setOIDCToken(user, session.getId(), token);
            } else {
                log.error("User object not found in session " + session.getId() + " ["
                        + Integer.toHexString(session.hashCode()) + "]");
            }
            if (redirect == null || REQUEST_UMA_ALSO_WITH_REDIRECT) {
                if (log.isDebugEnabled()) {
                    log.debug("Getting current infrastructure context via portal context class");
                }
                String currentContext = "/" + PortalContext.getConfiguration().getInfrastructureName();
                OIDCUmaUtil.checkUMATicketAndProvideInThreadLocal(request, response, user, session, currentContext);
            } else if (log.isDebugEnabled()) {
                log.debug("UMA token will be set by the valve after the redirection to: " + redirect);
            }
        } else {
            if (token == null) {
                log.error("OIDC token object is null in request");
            }
            if (session == null) {
                log.error("Session is null");
            }
        }

        if (redirect != null) {
            // Checking if a redirect has been already set (e.g. after an exception and/or a redirect to the logout URI)
            if (!response.isCommitted()) {
                if (log.isDebugEnabled()) {
                    log.debug("Redirecting to the original requested URI: " + redirect);
                }
                try {
                    // I'm not sure I can use this LR facility since it's used also by landing-page-hook.
                    // Indeed perhaps it should also be discussed if it takes precedence over this redirect in the case.
                    // session.setAttribute(WebKeys.LAST_PATH, new LastPath(null, URLDecoder.decode(redirect, "UTF-8"))
                    response.sendRedirect(URLDecoder.decode(redirect, "UTF-8"));
                } catch (IOException e) {
                    new ActionException("Redirecting to original requested URI: " + redirect, e);
                }
            } else {
                log.warn("Cannot redirect to original redirect URI (" + redirect + ") since the response is already commited");
            }
        } else if (log.isDebugEnabled()) {
            log.debug("No original requested URI has been found in session");
        }
    }

}
