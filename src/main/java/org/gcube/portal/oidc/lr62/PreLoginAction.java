package org.gcube.portal.oidc.lr62;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class PreLoginAction extends Action {

	protected static final Log log = LogFactoryUtil.getLog(PreLoginAction.class);

    @Override
    public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        if (log.isTraceEnabled()) {
            log.trace("PreLoginAction invoked");
        }
        // Noting to do at the moment
    }

}
