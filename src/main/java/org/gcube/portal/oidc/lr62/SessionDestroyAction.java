package org.gcube.portal.oidc.lr62;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.gcube.oidc.rest.JWTToken;
import org.gcube.oidc.rest.OpenIdConnectRESTHelper;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SessionAction;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;

public class SessionDestroyAction extends SessionAction {

    protected static final Log log = LogFactoryUtil.getLog(SessionDestroyAction.class);

    @Override
    public void run(HttpSession session) throws ActionException {
        if (log.isTraceEnabled()) {
            log.trace("Session details " + session.getId() + " [" + Integer.toHexString(session.hashCode()) + "]");
        }
        User user = (User) session.getAttribute(WebKeys.USER);
        LiferayOpenIdConnectConfiguration configuration = LiferayOpenIdConnectConfiguration.getConfiguration();
        if (configuration.logoutOnPortalLogout()) {
            // Getting the token from the cache proxy because it can be changed due to the (multiple) refresh
            JWTToken token = JWTCacheProxy.getInstance().getOIDCToken(user, session.getId());
            if (token != null) {
                if (log.isDebugEnabled()) {
                    log.debug("Performing logout on OIDC server due to session destroy for user: "
                            + user.getScreenName());
                }
                try {
                    if (log.isDebugEnabled()) {
                        log.debug("Performing the OIDC backchannel logout");
                    }
                    OpenIdConnectRESTHelper.logout(configuration.getLogoutURL(), token);
                } catch (IOException e) {
                    throw new ActionException("Performing logut on OIDC server", e);
                } finally {
                    if (log.isInfoEnabled()) {
                        log.info("Removing OIDC tokens from cache proxy for user " + user.getScreenName() + " and session "
                                + session.getId());
                    }
                    JWTCacheProxy.getInstance().removeOIDCToken(user, session.getId());
                    JWTCacheProxy.getInstance().removeUMAToken(user, session.getId());
                }
            } else {
                log.warn("Cannot find the OIDC token in session " + session.getId() + " ["
                        + Integer.toHexString(session.hashCode()) + "]");
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Don't perform OIDC logout according to configuration for user: " + user.getScreenName());
            }
        }
    }

}
