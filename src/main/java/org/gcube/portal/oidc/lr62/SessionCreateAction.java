package org.gcube.portal.oidc.lr62;

import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SessionAction;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class SessionCreateAction extends SessionAction {

	protected static final Log log = LogFactoryUtil.getLog(SessionCreateAction.class);

	@Override
	public void run(HttpSession session) throws ActionException {
		if (log.isTraceEnabled()) {
			log.trace("Session created. Details: id=" + session.getId() + ", instance=" + session);
		}
		// Noting to do at the moment
	}

}
