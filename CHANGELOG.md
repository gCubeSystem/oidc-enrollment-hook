This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "oidc-enrollment-hook"

## [v1.2.1]
- Better handling of tokens remove from proxy in the `finally` statement in the `SessionDestroyAction` class
- Avatar for the user at login is now downloaded and saved/removed in a separate thread
- Redirect after the login to the original requested page is performed when response has not already commited due to redirect to another URI due to tokens expiration or errors

## [v1.2.0] - 2021-04-12
- Now an UMA token is issued also after the login in the `PostLoginAction` to assure token's presence for the context without have to wait the next HTTP call and the Valve's intervention, in the case when it is not necessary the redirect to an origin URI after the login. (#20591)

## [v1.1.3] - 2021-03-03
- Now user reconciliation/identification from OIDC token after the login is performed no more checking by using the email address but by using the User's username, the Liferay `screenname`. (#20827) (#20840)

## [v1.1.2] - 2021-01-20
- Added some info of the user is about to create in the logs expecially for screen name (auto-generated or externally provided) (#20413). Restored per-session token removal. Logs revised. (#20445)

## [v1.1.1]
- Fixed occasionally missing of the redirect to the original requested URI after OIDC login

## [v1.1.0]
- Added avatar retrieve/update at every login (#19726)

## [v1.0.0]
- First release (#19143) (#19225) (#19226) (#19227) (#19228)
